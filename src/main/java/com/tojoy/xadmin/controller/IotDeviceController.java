package com.tojoy.xadmin.controller;

import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.R;
import com.tojoy.xadmin.entity.IotDeviceEntity;
import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.service.IIotDeviceService;
import com.tojoy.xadmin.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.controller
 * @ClassName: IotDeviceController
 * @create: 2021-10-26 13:58
 **/
@Controller
@RequestMapping("/device")
public class IotDeviceController {
    @Autowired
    IIotDeviceService iotDeviceService;

    /**
     * 跳转设备管理页面
     * @return
     */
    @RequestMapping("")
    public String toDeviceManagement() {
        return "device/deviceList";
    }

    /**
     * 跳转添加设备页面
     * @return 页面url
     */
    @RequestMapping("/addDevice")
    public String addDevice() {
        return "device/addDevice";
    }

    /**
     * 跳转添启动页面
     * @return 页面url
     */
    @RequestMapping("/startDevice")
    public String start() {
        return "hello";
    }

    @RequestMapping("/deviceDetails")
    public String deviceDetails() {
        return "device/deviceDetails";
    }



    /**
     * 获取设备列表
     * @param session session会话
     * @return 设备列表
     */
    @RequestMapping("/getDeviceList")
    @ResponseBody
    public Result<Object> getDeviceList(HttpSession session, @RequestParam Map<String, Object> params) {
        UserEntity userEntity = (UserEntity) session.getAttribute("userInfo");
        if (userEntity != null) {
            PageUtils iotDevicePage = iotDeviceService.queryPage(params);
            return Result.success(iotDevicePage.getList(), (long) iotDevicePage.getTotalCount());
        }
        else return Result.fail("请登录后重试");
    }


    /**
     * 列表
     * @param params
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = iotDeviceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{deviceId}")
    @ResponseBody
    public R info(@PathVariable("deviceId") Long deviceId){
        IotDeviceEntity device = iotDeviceService.getById(deviceId);

        return R.ok().put("device", device);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @ResponseBody
    public R save(@RequestBody IotDeviceEntity device){
        iotDeviceService.save(device);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @ResponseBody
    public R update(@RequestBody IotDeviceEntity device){
        iotDeviceService.updateById(device);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public R delete(@RequestBody Long[] deviceIds){
        iotDeviceService.removeByIds(Arrays.asList(deviceIds));

        return R.ok();
    }

    @DeleteMapping("/delete/{ids}")
    @ResponseBody
    public Result<Object> deleteDevice(@PathVariable("ids") List<Integer> idsList) {
        Boolean remove = iotDeviceService.removeDevice(idsList);
        if (remove) {
            return Result.success("删除成功");
        } else {
            return Result.fail("删除失败");
        }

    }

}
