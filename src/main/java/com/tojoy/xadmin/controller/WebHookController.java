package com.tojoy.xadmin.controller;

import com.tojoy.xadmin.service.impl.IotDeviceServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.controller
 * @ClassName: WebHookController
 * @create: 2021-10-21 09:17
 **/
@RestController
@RequestMapping("/mqtt")
public class WebHookController {
    private static final Logger log = LoggerFactory.getLogger(WebHookController.class);

    @Autowired
    IotDeviceServiceImpl iotDeviceService;
    private Map<String, Boolean> clientStatusMap = new HashMap<>();

    @RequestMapping("/webhook")
    public void Hook(@RequestBody Map<String, Object> params) {
        log.info("emqx 触发 webhook,请求体数据={}", params);
        String action = (String) params.get("action");

        String clientId = (String) params.get("clientid");
        if (action.equals("client_connected")) {
        //客户端成功接入
            clientStatusMap.put(clientId, true);
            iotDeviceService.getDeviceOnline(clientId);
        }
        if (action.equals("client_disconnected")) {
//客户端断开连接
            clientStatusMap.put(clientId, false);
        }
    }
    @GetMapping("/getall")
    public Map getAllStatus(){
        return clientStatusMap;
    }

}
