package com.tojoy.xadmin.controller;

import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.R;
import com.tojoy.xadmin.entity.LogoutEntity;
import com.tojoy.xadmin.service.ILogoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 * 运行记录
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@Controller
@RequestMapping("/logout")
public class LogoutController {
    @Autowired
    private ILogoutService ILogoutService;

    /**
     * 运行记录
     * @return
     */
    @RequestMapping("")
    public String logout() {
        return "record/operationRecord";
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @ResponseBody
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = ILogoutService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @ResponseBody
    public R info(@PathVariable("id") Integer id){
		LogoutEntity logout = ILogoutService.getById(id);

        return R.ok().put("logout", logout);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @ResponseBody
    public R save(@RequestBody LogoutEntity logout){
		ILogoutService.save(logout);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @ResponseBody
    public R update(@RequestBody LogoutEntity logout){
		ILogoutService.updateById(logout);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public R delete(@RequestBody Integer[] ids){
		ILogoutService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
