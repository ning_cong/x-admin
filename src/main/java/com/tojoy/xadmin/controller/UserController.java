package com.tojoy.xadmin.controller;

import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.service.IUserService;
import com.tojoy.xadmin.vo.Result;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.controller
 * @ClassName: UserController
 * @create: 2021-10-12 13:04
 **/
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @PostMapping("/login")
    public Result<Object> login(UserEntity param, @RequestParam("captcha") String captcha, HttpServletRequest request, HttpSession httpSession) {
        if (!CaptchaUtil.ver(captcha,request)){
            return Result.fail("验证码错误");
        }
        UserEntity userEntity = userService.login(param);
        if (userEntity != null) {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            boolean matches = passwordEncoder.matches(param.getPassword(), userEntity.getPassword());
            if (matches) {
                System.out.println("匹配成功");
                userEntity.setPassword(null);
                httpSession.setAttribute("userInfo", userEntity);
                return Result.success();
            }
            return Result.fail("password error");
        }
        return Result.fail();
    }
}
