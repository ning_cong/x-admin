package com.tojoy.xadmin.controller;

import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.R;
import com.tojoy.xadmin.entity.HistoryRecordEntity;
import com.tojoy.xadmin.service.IHistoryRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 *
 * 历史记录
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@Controller
@RequestMapping("/historyrecord")
public class HistoryRecordController {
    @Autowired
    private IHistoryRecordService IHistoryRecordService;

    /**
     * 历史记录
     * @return 视图
     */
    @RequestMapping("")
    public String historyrecord() {
        return "record/historyRecord";
    }

    /**
     * 历史记录曲线图
     * @return 视图
     */
    @RequestMapping("/chart")
    public String historyRecordCharts() {
        return "record/historyRecordChart";
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    @ResponseBody
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = IHistoryRecordService.queryHistoryPage(params);
        R r = R.ok();
        r.put("data", page.getList());
        r.put("count", page.getTotalCount());
        return r;
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @ResponseBody
    public R info(@PathVariable("id") Integer id){
        HistoryRecordEntity historyRecord = IHistoryRecordService.getById(id);

        return R.ok().put("historyRecord", historyRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @ResponseBody
    public R save(@RequestBody HistoryRecordEntity historyRecord){
        IHistoryRecordService.save(historyRecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @ResponseBody
    public R update(@RequestBody HistoryRecordEntity historyRecord){
        IHistoryRecordService.updateById(historyRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public R delete(@RequestBody Integer[] ids){
        IHistoryRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
