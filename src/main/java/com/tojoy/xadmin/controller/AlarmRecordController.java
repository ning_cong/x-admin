package com.tojoy.xadmin.controller;

import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.R;
import com.tojoy.xadmin.entity.AlarmRecordEntity;
import com.tojoy.xadmin.service.IAlarmRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 
 * 故障记录
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@RestController
@RequestMapping("/alarmrecord")
public class AlarmRecordController {
    @Autowired
    private IAlarmRecordService IAlarmRecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        for(String key : params.keySet()){
            String value = (String) params.get(key);
            System.out.println(key+":"+value);
        }
        PageUtils page = IAlarmRecordService.queryPage(params);
        List<?> list = page.getList();
        R r = R.ok();
        r.put("data", list);
        r.put("count", list.size());
        return r;
    }



    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		AlarmRecordEntity alarmRecord = IAlarmRecordService.getById(id);

        return R.ok().put("alarmRecord", alarmRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AlarmRecordEntity alarmRecord){
		IAlarmRecordService.save(alarmRecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AlarmRecordEntity alarmRecord){
		IAlarmRecordService.updateById(alarmRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		IAlarmRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
