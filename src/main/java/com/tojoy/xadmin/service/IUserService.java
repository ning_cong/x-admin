package com.tojoy.xadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.entity.UserEntity;

import java.util.Map;

/**
 * created with IntelliJ IDEA.
 *
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName:
 * @ClassName: UserService
 * @create: 2021-10-12 11:53
 * @description:
 **/
public interface IUserService extends IService<UserEntity> {

    public UserEntity login(UserEntity userEntity);

    PageUtils queryPage(Map<String, Object> params);
}
