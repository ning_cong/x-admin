package com.tojoy.xadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.entity.IotDeviceStatusEntity;

import java.util.Map;

/**
 * 
 *
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-10-28 16:24:07
 */
public interface IIotDeviceStatusService extends IService<IotDeviceStatusEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

