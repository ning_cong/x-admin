package com.tojoy.xadmin.service;

import com.tojoy.xadmin.emp.entity.Dept;
import com.tojoy.xadmin.emp.entity.Emp;
import com.tojoy.xadmin.emp.vo.EmpQuery;

import java.util.List;

/**
 * created with IntelliJ IDEA.
 *
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName:
 * @ClassName: EmpService
 * @create: 2021-10-12 16:25
 * @description:
 **/
public interface IEmpService {
    List<Emp> getEmpList(EmpQuery param);

    Long countEmpList(EmpQuery param);

    int addEmp(Emp emp);

    List<Dept> getDept();

    void deleteEmpByIds(String ids);

    Emp getEmpById(Integer id);

    void editEmp(Emp emp);

}
