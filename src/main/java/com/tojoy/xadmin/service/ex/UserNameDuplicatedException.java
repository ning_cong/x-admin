package com.tojoy.xadmin.service.ex;

/**
 * @description: 用户被占用异常类
 * @author: qnc
 * @Version: 1.0
 * @program: store
 * @packageName: com.tojoy.store.service.ex
 * @ClassName: UserNameDuplicatedException
 * @create: 2021-10-14 13:09
 **/
public class UserNameDuplicatedException extends ServiceException{
    public UserNameDuplicatedException() {
    }

    public UserNameDuplicatedException(String message) {
        super(message);
    }

    public UserNameDuplicatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNameDuplicatedException(Throwable cause) {
        super(cause);
    }

    public UserNameDuplicatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
