package com.tojoy.xadmin.service.ex;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: store
 * @packageName: com.tojoy.store.service.ex
 * @ClassName: UpdateException
 * @create: 2021-10-15 08:35
 **/
public class UpdateException extends ServiceException{
    public UpdateException() {
    }

    public UpdateException(String message) {
        super(message);
    }

    public UpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateException(Throwable cause) {
        super(cause);
    }

    public UpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
