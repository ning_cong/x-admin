package com.tojoy.xadmin.service.ex;

/**
 * @description: 数据插入时产生的异常
 * @author: qnc
 * @Version: 1.0
 * @program: store
 * @packageName: com.tojoy.store.service.ex
 * @ClassName: InsertException
 * @create: 2021-10-14 13:10
 **/
public class InsertException extends ServiceException{
    public InsertException() {
    }

    public InsertException(String message) {
        super(message);
    }

    public InsertException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsertException(Throwable cause) {
        super(cause);
    }

    public InsertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
