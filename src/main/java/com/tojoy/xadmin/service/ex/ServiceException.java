package com.tojoy.xadmin.service.ex;

/**
 * @description: 业务层异常基类
 * @author: qnc
 * @Version: 1.0
 * @program: store
 * @packageName: com.tojoy.store.service.ex
 * @ClassName: ServiceException
 * @create: 2021-10-14 13:06
 **/
public class ServiceException extends RuntimeException {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    protected ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
