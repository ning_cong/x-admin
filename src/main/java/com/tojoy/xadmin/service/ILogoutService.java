package com.tojoy.xadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.entity.LogoutEntity;

import java.util.Map;

/**
 * 
 *
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
public interface ILogoutService extends IService<LogoutEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

