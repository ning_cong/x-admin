package com.tojoy.xadmin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.Query;
import com.tojoy.xadmin.entity.LogoutEntity;
import com.tojoy.xadmin.mapper.record.LogoutMapper;
import com.tojoy.xadmin.service.ILogoutService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("logoutService")
public class ILogoutServiceImpl extends ServiceImpl<LogoutMapper, LogoutEntity> implements ILogoutService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<LogoutEntity> page = this.page(
                new Query<LogoutEntity>().getPage(params),
                new QueryWrapper<LogoutEntity>()
        );

        return new PageUtils(page);
    }

}