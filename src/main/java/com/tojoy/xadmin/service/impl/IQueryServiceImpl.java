package com.tojoy.xadmin.service.impl;

import com.tojoy.xadmin.service.IQueryService;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.service.serviceImpl
 * @ClassName: IQueryServiceImpl
 * @create: 2021-10-22 09:17
 **/
public class IQueryServiceImpl implements IQueryService {
    @Override
    public void getHistoryRecord() {

    }

    @Override
    public void searchHistoryRecord() {

    }

    @Override
    public void removeHistoryRecord() {

    }

    @Override
    public void getRuntimeData() {

    }

    @Override
    public void getDeviceSetting() {

    }

    @Override
    public void getOnlineDevice() {

    }

    @Override
    public void getOnlineDeviceId() {

    }

    @Override
    public void getErrorRecords() {

    }
}
