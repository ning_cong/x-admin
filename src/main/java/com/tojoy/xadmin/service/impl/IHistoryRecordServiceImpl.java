package com.tojoy.xadmin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.Query;
import com.tojoy.xadmin.entity.HistoryRecordEntity;
import com.tojoy.xadmin.mapper.record.HistoryRecordMapper;
import com.tojoy.xadmin.service.IHistoryRecordService;
import com.tojoy.xadmin.vo.HistoryRecordVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("historyRecordService")
public class IHistoryRecordServiceImpl extends ServiceImpl<HistoryRecordMapper, HistoryRecordEntity> implements IHistoryRecordService {
    @Autowired
    HistoryRecordMapper historyRecordMapper;



    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<HistoryRecordEntity> page = this.page(
                new Query<HistoryRecordEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryHistoryPage(Map<String, Object> params) {
        QueryWrapper<HistoryRecordEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(params.containsKey("deviceId") && StringUtils.isNotBlank((String) params.get("deviceId")),"thr.device_id",params.get("deviceId"));
        queryWrapper.ge(params.containsKey("startDate") && StringUtils.isNotBlank((String) params.get("startDate")),"thr.start_time",params.get("startDate"));
        queryWrapper.le(params.containsKey("endDate") && StringUtils.isNotBlank((String) params.get("endDate")),"thr.start_time",params.get("endDate"));
        IPage<HistoryRecordVo> historyRecordVoIPage = historyRecordMapper.selectHistoryPage(new Query<HistoryRecordEntity>().getPage(params),queryWrapper);
        return new PageUtils(historyRecordVoIPage);
    }

}