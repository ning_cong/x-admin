package com.tojoy.xadmin.service.impl;

import com.tojoy.xadmin.emp.entity.Dept;
import com.tojoy.xadmin.emp.entity.Emp;
import com.tojoy.xadmin.emp.vo.EmpQuery;
import com.tojoy.xadmin.mapper.DepMapper;
import com.tojoy.xadmin.mapper.EmpMapper;
import com.tojoy.xadmin.service.IEmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.service.serviceImpl
 * @ClassName: EmpServiceImpl
 * @create: 2021-10-12 16:28
 **/
@Service
public class EmpServiceImpl implements IEmpService {
    @Autowired
    EmpMapper empMapper;

    @Autowired
    DepMapper depMapper;

    @Override
    public List<Emp> getEmpList(EmpQuery param) {
        return empMapper.getEmpList(param);
    }

    @Override
    public Long countEmpList(EmpQuery param) {
        return empMapper.countEmpList(param);
    }

    @Override
    public int addEmp(Emp emp) {
        return empMapper.insert(emp);
    }

    @Override
    public List<Dept> getDept() {
        return depMapper.selectList(null);
    }

    @Override
    public void deleteEmpByIds(String ids) {
        empMapper.deleteEmpByIds(ids);
    }

    @Override
    public Emp getEmpById(Integer id) {
        return empMapper.selectById(id);
    }

    /**
     * 修改员工信息服务实现
     * @param emp
     */
    @Override
    public void editEmp(Emp emp) {
        empMapper.updateById(emp);
    }
}
