package com.tojoy.xadmin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.Query;
import com.tojoy.xadmin.entity.AlarmRecordEntity;
import com.tojoy.xadmin.mapper.record.AlarmRecordMapper;
import com.tojoy.xadmin.service.IAlarmRecordService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("alarmRecordService")
public class IAlarmRecordServiceImpl extends ServiceImpl<AlarmRecordMapper, AlarmRecordEntity> implements IAlarmRecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<AlarmRecordEntity> queryWrapper = new QueryWrapper<>();

        queryWrapper.like(params.containsKey("errorCode") && !(params.get("errorCode").equals("")),"error_code",params.get("errorCode"))
                .like(params.containsKey("errorType")  && !(params.get("errorType").equals("")),"occur_reason",params.get("errorType"))
                .ge(params.containsKey("startDate") && !(params.get("startDate").equals("")),"occur_time",params.get("startDate"))
                .le(params.containsKey("endDate")  && !(params.get("endDate").equals("")),"occur_time",params.get("endDate"));

        IPage<AlarmRecordEntity> page = this.page(
                new Query<AlarmRecordEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}