package com.tojoy.xadmin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.Query;
import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.mapper.UserMapper;
import com.tojoy.xadmin.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.service.serviceImpl
 * @ClassName: UserServiceImpl
 * @create: 2021-10-12 11:54
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserEntity login(UserEntity userEntity) {
        return userMapper.getUser(userEntity);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                new QueryWrapper<UserEntity>()
        );

        return new PageUtils(page);
    }
}
