package com.tojoy.xadmin.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tojoy.xadmin.common.DeviceChannelMap;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.Query;
import com.tojoy.xadmin.emp.vo.EmpQuery;
import com.tojoy.xadmin.entity.IotDeviceEntity;
import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.mapper.UserMapper;
import com.tojoy.xadmin.mapper.device.IotDeviceMapper;
import com.tojoy.xadmin.service.IIotDeviceService;
import com.tojoy.xadmin.vo.IotDeviceOnlineStatus;
import com.tojoy.xadmin.vo.IotDeviceVo;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.service.serviceImpl
 * @ClassName: IotDeviceServie
 * @create: 2021-10-21 10:15
 **/
@Service
public class IotDeviceServiceImpl extends ServiceImpl<IotDeviceMapper,IotDeviceEntity> implements IIotDeviceService {

    Map<String, ChannelHandlerContext> channelUserMap = DeviceChannelMap.getMAP();

    List<IotDeviceEntity> iotOnlineDeviceList = DeviceChannelMap.getIotDeviceList();
    @Autowired
    IotDeviceMapper iotDeviceMapper;
    @Autowired
    UserMapper userMapper;

    /**
     * 设备在线状态实现
     * @param deviceInfo 设备信息
     */
    @Override
    public void getDeviceOnline(String deviceInfo) {
        IotDeviceOnlineStatus deviceOnlineStatus = JSON.parseObject(deviceInfo, IotDeviceOnlineStatus.class);
        //创建条件构造器
        QueryWrapper<IotDeviceEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("device_num",deviceOnlineStatus.getClientId());
        IotDeviceEntity iotDeviceEntity = iotDeviceMapper.selectOne(wrapper);
        //更新数据库
        if (iotDeviceEntity != null) {
            iotOnlineDeviceList.add(iotDeviceEntity);
            ChannelHandlerContext ctx = channelUserMap.get("1");
            ctx.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(iotDeviceEntity.getOwnerId())));
        }
    }

    /**
     * 设备下线提示实现类
     * @param deviceInfo 设备下线提示
     */
    @Override
    public void getDeviceOffline(String deviceInfo) {
        IotDeviceOnlineStatus device = JSON.parseObject(deviceInfo, IotDeviceOnlineStatus.class);
        ChannelHandlerContext ctx = channelUserMap.get("1");
        ctx.writeAndFlush(new TextWebSocketFrame("设备下线"));
    }

    /**
     * 获取设备参数实现类
     * @param deviceInfo 设备信息
     */
    @Override
    public void getDeviceParams(String deviceInfo) {
        //数据转换成对象
        IotDeviceEntity device = JSON.parseObject(deviceInfo, IotDeviceEntity.class);
        //查询数据库中的设备信息
        QueryWrapper<IotDeviceEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("device_num",device.getDeviceNum());
        IotDeviceEntity deviceEntity = iotDeviceMapper.selectOne(wrapper);
        //更新数据字段并更新数据库
        device.setDeviceId(deviceEntity.getDeviceId());
        iotDeviceMapper.updateById(deviceEntity);
    }


    @Override
    public void DeviceStart() {

    }

    @Override
    public void deviceStop() {

    }

    @Override
    public void uploadRunTimeData() {

    }

    /**
     * 获取所有设备实现类
     * @param param 设备信息
     * @param userEntity 设备信息
     * @return 设备列表
     */
    @Override
    public List<IotDeviceVo> getDeviceVoList(EmpQuery param, UserEntity userEntity) {
        QueryWrapper<UserEntity> userQueryWrapper = new QueryWrapper<>();
        String username = userEntity.getUsername();
        userQueryWrapper.eq("username",username);
        UserEntity resultUserEntity = userMapper.selectOne(userQueryWrapper);
        if (resultUserEntity ==null) {
        }
        List<IotDeviceVo> iotDeviceVoList = iotDeviceMapper.selectIotDeviceVo(resultUserEntity);
        if (iotDeviceVoList == null){
        }

        return iotDeviceVoList;
    }


    @Override
    public List<IotDeviceEntity> getDeviceList(EmpQuery param, UserEntity userEntity) {
        QueryWrapper<UserEntity> userQueryWrapper = new QueryWrapper<>();
        String username = userEntity.getUsername();
        userQueryWrapper.eq("username",username);
        UserEntity resultUserEntity = userMapper.selectOne(userQueryWrapper);
        if (resultUserEntity ==null) {
        }
        QueryWrapper<IotDeviceEntity> deviceQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("owner_id", resultUserEntity.getId());
        List<IotDeviceEntity> iotDeviceEntityList = iotDeviceMapper.selectList(deviceQueryWrapper);
        if (iotDeviceEntityList == null){
        }

        return iotDeviceEntityList;
    }
    @Override
    public Long getDeviceNum(EmpQuery param, UserEntity userEntity) {
        QueryWrapper<IotDeviceEntity> wrapper = new QueryWrapper<>();
        QueryWrapper<UserEntity> userQueryWrapper = new QueryWrapper<>();
        String username = userEntity.getUsername();
        userQueryWrapper.eq("username",username);
        UserEntity resultUserEntity = userMapper.selectOne(userQueryWrapper);
        if (resultUserEntity ==null) {
        }
        QueryWrapper<IotDeviceEntity> deviceQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("owner_id", resultUserEntity.getId());
        return iotDeviceMapper.selectCount(deviceQueryWrapper);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<IotDeviceEntity> page = this.page(
                new Query<IotDeviceEntity>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    @Override
    public Boolean removeDevice(List<Integer> ids) {
        return this.removeByIds(ids);
    }
}
