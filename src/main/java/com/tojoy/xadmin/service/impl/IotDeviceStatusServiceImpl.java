package com.tojoy.xadmin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.common.util.Query;
import com.tojoy.xadmin.entity.IotDeviceStatusEntity;
import com.tojoy.xadmin.mapper.device.IotDeviceStatusMapper;
import com.tojoy.xadmin.service.IIotDeviceStatusService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class IotDeviceStatusServiceImpl extends ServiceImpl<IotDeviceStatusMapper, IotDeviceStatusEntity> implements IIotDeviceStatusService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<IotDeviceStatusEntity> page = this.page(
                new Query<IotDeviceStatusEntity>().getPage(params),
                new QueryWrapper<IotDeviceStatusEntity>()
        );

        return new PageUtils(page);
    }

}