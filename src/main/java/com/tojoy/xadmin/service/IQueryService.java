package com.tojoy.xadmin.service;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.service.serviceImpl
 * @ClassName: QueryService
 * @create: 2021-10-22 09:16
 **/
public interface IQueryService {

    /**
     * 查询历史记录
     */
    void getHistoryRecord();


    /**
     * 搜索历史记录
     */
    void searchHistoryRecord();


    /**
     * 删除历史记录
     */
    void removeHistoryRecord();

    /**
     * 查询运行数据
     */
    void getRuntimeData();

    /**
     * 查询设备参数配置信息
     */
    void getDeviceSetting();

    /**
     * 查询在线设备
     */
    void getOnlineDevice();

    /**
     * 查询在线设备id
     */
    void getOnlineDeviceId();

    /**
     * 查询故障记录
     */
    void getErrorRecords();
}
