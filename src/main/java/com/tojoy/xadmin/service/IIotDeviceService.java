package com.tojoy.xadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.emp.vo.EmpQuery;
import com.tojoy.xadmin.entity.IotDeviceEntity;
import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.vo.IotDeviceVo;

import java.util.List;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 *
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName:
 * @ClassName: IIotDeviceService
 * @create: 2021-10-21 08:30
 * @description:
 **/
public interface IIotDeviceService extends IService<IotDeviceEntity> {

    /**
     * 发送设备上线信息
     */
    void getDeviceOnline(String deviceInfo);

    /**
     * 设备下线
     * @param
     */
    void getDeviceOffline(String deviceInfo);


    /**
     * 设备参数
     *
     */
    void getDeviceParams(String deviceInfo);


    /**
     * 设备启动
     */
    void DeviceStart();

    /**
     * 设备停止
     */
    void deviceStop();

    /**
     * 运行时数据上传
     */

    void uploadRunTimeData();



    /**
     * 获取用户设备
     * @return
     * @param param
     * @param userEntity
     */
    List<IotDeviceEntity> getDeviceList(EmpQuery param, UserEntity userEntity);


    /**
     * 获取用户设备及初始状态
     * @return
     * @param param
     * @param userEntity
     */
    List<IotDeviceVo> getDeviceVoList(EmpQuery param, UserEntity userEntity);
    /**
     * 获取用户设备数量
     * @return 设备数量
     * @param param
     * @param userEntity
     */
    Long getDeviceNum(EmpQuery param, UserEntity userEntity);


    /**
     * 分页
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);


    Boolean removeDevice(List<Integer> ids);
}



