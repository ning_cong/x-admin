package com.tojoy.xadmin.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.entity
 * @ClassName: IotDeviceStatus
 * @create: 2021-10-28 16:18
 **/
@Data
@TableName("t_device_status")
public class IotDeviceStatusEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Integer id;
    /**
     * 设备id
     */
    private Integer deviceId;
    /**
     * 转子号
     */
    private Integer rotorNum;
    /**
     * 速度
     */
    private Double speed;
    /**
     * 离心力
     */
    private String centrifugalForce;
    /**
     * 时间
     */

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date time;
    /**
     * 温度
     */
    private String temperature;

}

