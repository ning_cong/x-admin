/******************************************************************************
 * 作者：kerwincui
 * 时间：2021-06-08
 * 邮箱：164770707@qq.com
 * 源码地址：https://gitee.com/kerwincui/wumei-smart
 * author: kerwincui
 * create: 2021-06-08
 * email：164770707@qq.com
 * source:https://github.com/kerwincui/wumei-smart
 ******************************************************************************/
package com.tojoy.xadmin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设备对象 iot_device
 * 
 * @author kerwincui
 * @date 2021-05-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_device")
public class IotDeviceEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId(type = IdType.AUTO)
    private Long deviceId;

    /**
     * 编号
     */
    private String deviceNum;


    /**
     * 名称
     */
    private String deviceName;


    /**
     * 用户
     */
    private String ownerId;


    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @TableLogic(value = "0",delval = "1")
    private String isDeleted;


}