package com.tojoy.xadmin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * 
 * 
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@Data
@TableName("t_history_record")
public class HistoryRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 
	 */
	private Integer deviceId;
	/**
	 * 
	 */
	private String programType;
	/**
	 * 
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	/**
	 * 
	 */
	private Time workTime;
	/**
	 * 
	 */
	private Double speed;
	/**
	 * 
	 */
	private String centrifugalForce;
	/**
	 * 
	 */
	private String temperature;
	/**
	 * 
	 */
	private String runStatus;

}
