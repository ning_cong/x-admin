package com.tojoy.xadmin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@Data
@TableName("t_logout")
public class LogoutEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer deviceId;
	/**
	 * 
	 */
	private String cumulativeTime;
	/**
	 * 
	 */
	private Integer numberLock;
	/**
	 * 
	 */
	private Integer alarmRecordId;

}
