package com.tojoy.xadmin.handle.mqtthandler;

import com.tojoy.xadmin.service.IIotDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @program: spring-boot-mqtt-test
 * @ClassName MqttReceiveHandle
 * @description: MQTT消息处理类
 * @author: qnc
 * @create: 2021-08-09 10:40
 * @Version 1.0
 **/
@Slf4j
@Component
public class MqttReceiveHandle {

    @Autowired
    IIotDeviceService iotDeviceService;

    public void handle(Message<?> message){
        log.info(message.toString());
        //log.info("主题：{}，QOS:{}，消息接收到的数据：{}", message.getHeaders().get(MqttHeaders.RECEIVED_TOPIC), message.getHeaders().get(MqttHeaders.RECEIVED_QOS), message.getPayload());
        if (Objects.equals(Objects.requireNonNull(message.getHeaders().get(MqttHeaders.RECEIVED_TOPIC)).toString(), "tojoy/equipments/online")) {
            iotDeviceService.getDeviceOnline(message.getPayload().toString());

        }
        else if (Objects.equals(message.getHeaders().get(MqttHeaders.RECEIVED_TOPIC), "tojoy/equipments/offline")) {
            iotDeviceService.getDeviceOffline(message.getPayload().toString());
        }
        else if (message.getHeaders().get(MqttHeaders.RECEIVED_TOPIC) == "tojoy/equipments/equipment_args"){
            iotDeviceService.getDeviceParams(message.getPayload().toString());
        }
    }
}
