package com.tojoy.xadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tojoy.xadmin.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.mapper
 * @ClassName: UserMapper
 * @create: 2021-10-12 11:50
 **/
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
    public UserEntity getUser(UserEntity userEntity);
}
