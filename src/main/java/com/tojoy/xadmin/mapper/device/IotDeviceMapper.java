package com.tojoy.xadmin.mapper.device;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tojoy.xadmin.entity.IotDeviceEntity;
import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.vo.IotDeviceVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * created with IntelliJ IDEA.
 *
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName:
 * @ClassName: IotDeviceMapper
 * @create: 2021-10-21 10:39
 * @description:
 **/
@Mapper
public interface IotDeviceMapper extends BaseMapper<IotDeviceEntity> {
    public List<IotDeviceVo> selectIotDeviceVo(UserEntity user);
}
