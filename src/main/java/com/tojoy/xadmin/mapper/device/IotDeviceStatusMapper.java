package com.tojoy.xadmin.mapper.device;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tojoy.xadmin.entity.IotDeviceStatusEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-10-28 16:24:07
 */
@Mapper
public interface IotDeviceStatusMapper extends BaseMapper<IotDeviceStatusEntity> {
	
}
