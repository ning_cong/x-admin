package com.tojoy.xadmin.mapper.record;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.tojoy.xadmin.entity.HistoryRecordEntity;
import com.tojoy.xadmin.vo.HistoryRecordVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@Mapper
public interface HistoryRecordMapper extends BaseMapper<HistoryRecordEntity> {
    IPage<HistoryRecordVo> selectHistoryPage(IPage<HistoryRecordEntity> page,@Param(Constants.WRAPPER) QueryWrapper<HistoryRecordEntity> wrapper);
}
