package com.tojoy.xadmin.mapper.record;

import com.tojoy.xadmin.entity.AlarmRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author qnc
 * @email 793266539@qq.com
 * @date 2021-11-05 10:17:04
 */
@Mapper
public interface AlarmRecordMapper extends BaseMapper<AlarmRecordEntity> {
	
}
