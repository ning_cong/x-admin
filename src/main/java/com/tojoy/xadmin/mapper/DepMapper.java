package com.tojoy.xadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tojoy.xadmin.emp.entity.Dept;
import org.apache.ibatis.annotations.Mapper;

/**
 * created with IntelliJ IDEA.
 *
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName:
 * @ClassName: DepMapper
 * @create: 2021-10-13 14:44
 * @description:
 **/
@Mapper
public interface DepMapper extends BaseMapper<Dept> {
}
