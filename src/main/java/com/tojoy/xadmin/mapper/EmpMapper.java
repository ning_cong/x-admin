package com.tojoy.xadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tojoy.xadmin.emp.entity.Emp;
import com.tojoy.xadmin.emp.vo.EmpQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * created with IntelliJ IDEA.
 *
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName:
 * @ClassName: EmpMapper
 * @create: 2021-10-12 16:28
 * @description:
 **/
@Mapper
public interface EmpMapper extends BaseMapper<Emp> {
    Long countEmpList(EmpQuery param);

    List<Emp> getEmpList(EmpQuery param);


    void deleteEmpByIds(String ids);
}
