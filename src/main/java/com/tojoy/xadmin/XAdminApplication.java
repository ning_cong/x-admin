package com.tojoy.xadmin;

import com.tojoy.xadmin.netty.MyNettyServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.tojoy.xadmin.mapper")
@SpringBootApplication
public class XAdminApplication implements CommandLineRunner {
    @Autowired
    private MyNettyServer myServer;

    public static void main(String[] args) {
        SpringApplication.run(XAdminApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        myServer.start(8888);
    }
}
