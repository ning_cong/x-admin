package com.tojoy.xadmin.dto;

import com.tojoy.xadmin.vo.Page;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.dto
 * @ClassName: PoerationRecordDto
 * @create: 2021-11-08 15:15
 **/
@Data
public class OperationRecordDto extends Page {
    private String errorCode;

    private String errorType;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date StartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
