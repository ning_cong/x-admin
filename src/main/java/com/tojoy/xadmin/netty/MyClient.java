package com.tojoy.xadmin.netty;/**
 * @author Qiu
 * @create 2021-07-29-9:14
 */

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @program: JavaSenior
 * @ClassName MyClient
 * @description:
 * @author: qnc
 * @create: 2021-07-29 09:14
 * @Version 1.0
 **/
@Component
@Slf4j
public class MyClient {
//    public static void main(String[] args) throws InterruptedException {
//        EventLoopGroup group = new NioEventLoopGroup();
//
//        try {
//            Bootstrap bootstrap = new Bootstrap();
//            bootstrap.group(group).channel(NioSocketChannel.class).handler(new MyClientInitializer());
//
//            ChannelFuture channelFuture = bootstrap.connect("127.0.0.1", 1903).sync();
//            channelFuture.channel().closeFuture().sync();
//        } finally {
//            group.shutdownGracefully();
//        }
//    }
    public void startNettyClient() throws InterruptedException {
        EventLoopGroup group = new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).channel(NioSocketChannel.class).handler(new MyClientInitializer());
            ChannelFuture channelFuture = bootstrap.connect("192.168.20.40", 23).sync();
            channelFuture.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully();
        }
    }
}
