package com.tojoy.xadmin.netty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: JavaSenior
 * @packageName: com.qnc.netty.tcp
 * @ClassName: MyClientInitializer
 * @create: 2021-09-13 14:54
 **/
public class MyClientInitializer extends ChannelInitializer<SocketChannel> {
    /**
     * 初始化客户端通道
     */
    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        // 消息解码:读取消息头和消息体
//        pipeline.addLast(new LengthFieldBasedFrameDecoder(1024, 0, 4, 0, 4));
//         //消息编码:将消息封装为消息头和消息体,在响应字节数据前面添加消息体长度
//        pipeline.addLast(new LengthFieldPrepender(4));
         //设置(字符串)编码器和解码器
        pipeline.addLast(new StringDecoder());
        pipeline.addLast( new StringEncoder());
        // 客户端连接成功之后的业务处理
        pipeline.addLast(new MyClientHandler());
    }
}
