package com.tojoy.xadmin.netty;

import com.tojoy.xadmin.handle.nettyhandler.WebSocketNettyHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: JavaSenior
 * @packageName: com.qnc.netty.tcp
 * @ClassName: MyServerInitializer
 * @create: 2021-09-13 15:04
 **/
@Component
public class MyServerInitializer extends ChannelInitializer<SocketChannel> {
    @Autowired
    WebSocketNettyHandler webSocketNettyHandler;
//    // 字符串编码器和解码器
//    private static final StringDecoder DECODER = new StringDecoder();
//    private static final StringEncoder ENCODER = new StringEncoder();
//    // 服务器端连接之后的执行器(自定义的类)
//    private static final MyServerHandler SERVER_HANDLER = new MyServerHandler();

    /**
     * 初始化通道的具体执行方法
     */
    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

        //因为基于http协议，使用http的编码和解码器
        pipeline.addLast(new HttpServerCodec());
        //是以块方式写，添加ChunkedWriteHandler处理器
        pipeline.addLast(new ChunkedWriteHandler());
                    /*
                    说明
                    1. http数据在传输过程中是分段, HttpObjectAggregator ，就是可以将多个段聚合
                    2. 这就就是为什么，当浏览器发送大量数据时，就会发出多次http请求
                    */
        pipeline.addLast(new HttpObjectAggregator(8192));
                    /*
                    说明
                    1. 对应websocket ，它的数据是以 帧(frame) 形式传递
                    2. 可以看到WebSocketFrame 下面有六个子类
                    3. 浏览器请求时 ws://localhost:7000/hello 表示请求的uri
                    4. WebSocketServerProtocolHandler 核心功能是将 http协议升级为 ws协议 , 保持长连接
                    5. 是通过一个 状态码 101
                    */
        pipeline.addLast(new WebSocketServerProtocolHandler("/hello2"));

        //自定义的handler ，处理业务逻辑
        pipeline.addLast(webSocketNettyHandler);
    }
}
