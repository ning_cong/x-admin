package com.tojoy.xadmin.emp.vo;

import com.tojoy.xadmin.vo.Page;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.emp.vo
 * @ClassName: EmpQuery
 * @create: 2021-10-12 16:23
 **/
@Data
public class EmpQuery extends Page {
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date StartDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
