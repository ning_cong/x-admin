package com.tojoy.xadmin.emp.controller;

import com.tojoy.xadmin.emp.entity.Dept;
import com.tojoy.xadmin.emp.entity.Emp;
import com.tojoy.xadmin.emp.vo.EmpQuery;
import com.tojoy.xadmin.service.IEmpService;
import com.tojoy.xadmin.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.emp.controller
 * @ClassName: EmpController
 * @create: 2021-10-12 16:21
 **/
@Controller
@RequestMapping("/emp")
public class EmpController {
    @Autowired
    IEmpService empService;

    @GetMapping("")
    public String toEmpListUI() {
        return "emp/deviceList";
    }

    @GetMapping("/list")
    @ResponseBody
    public Result<Object> getEmpList(EmpQuery param) {
        List<Emp> list = empService.getEmpList(param);
        Long count = empService.countEmpList(param);
        return Result.success(list, count);
    }


    @PostMapping("/addEmp")
    @ResponseBody
    public Result<Object> addEmp(Emp emp) {
        empService.addEmp(emp);
        return Result.success();
    }

    @GetMapping("/add/ui")
    public String toAddUI(Model model) {
        List<Dept> deptList = empService.getDept();
        model.addAttribute("deptList",deptList);
        return "emp/empAdd";
    }

    @DeleteMapping("/{ids}")
    @ResponseBody
    public Result<Object> deleteEmpById(@PathVariable("ids") String ids) {
        empService.deleteEmpByIds(ids);
        return Result.success("删除成功");
    }


    @GetMapping("/{id}")
    public String getEmpById(@PathVariable("id") Integer id, Model model){
        Emp emp = empService.getEmpById(id);
        model.addAttribute("emp",emp);
        model.addAttribute("deptList", empService.getDept());
        return "emp/empEdit";
    }

    @PutMapping("")
    @ResponseBody
    public Result<Object> editEmp(Emp emp) {
        empService.editEmp(emp);
        return Result.success("员工信息修改成功!");
    }
}
