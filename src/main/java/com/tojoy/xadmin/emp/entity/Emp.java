package com.tojoy.xadmin.emp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.emp.entity
 * @ClassName: Emp
 * @create: 2021-10-12 16:18
 **/
@Data
public class Emp implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer empId;
    private String name;
    private String sex;
    private Integer age;
    private Double sal;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String address;
    private Integer deptId;

    @TableField(exist = false)
    private Dept dept;
}
