package com.tojoy.xadmin.emp.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.emp.entity
 * @ClassName: Dept
 * @create: 2021-10-13 11:28
 **/
@Data
public class Dept implements Serializable {
    @TableId
    private Integer deptId;
    private String deptName;
}
