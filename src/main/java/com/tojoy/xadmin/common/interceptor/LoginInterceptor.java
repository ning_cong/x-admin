package com.tojoy.xadmin.common.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.common.interceptor
 * @ClassName: LoginInterceptor
 * @create: 2021-10-12 15:09
 **/
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取session并判断用户信息是否存在
        Object user = request.getSession().getAttribute("userInfo");
        if (user == null) {
            //未登录
            log.info("未登录，已被拦截"+request.getRequestURI());
            response.sendRedirect(request.getContextPath()+"/login");
            return false;
        }
        log.info("已登录，放行"+request.getRequestURI());
        return true;
    }
}
