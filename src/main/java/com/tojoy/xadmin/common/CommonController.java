package com.tojoy.xadmin.common;

import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.controller
 * @ClassName: CommonController
 * @create: 2021-10-12 11:43
 **/
@Controller
public class CommonController {
    /**
     * 登陆界面
     * @return 界面视图
     */
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * 主界面
     * @return 界面视图
     */
    @GetMapping("/index")
    public String index() {
        return "index";
    }


    /**
     * 验证码
     */
    @RequestMapping("/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CaptchaUtil.out(request,response);
    }


}
