package com.tojoy.xadmin.common.exception;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.common.exception
 * @ClassName: NotFoundController
 * @create: 2021-10-14 08:24
 **/
@Controller
public class NotFoundController implements ErrorController {
    
}
