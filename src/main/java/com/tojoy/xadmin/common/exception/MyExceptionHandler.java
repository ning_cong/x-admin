package com.tojoy.xadmin.common.exception;

import com.tojoy.xadmin.vo.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.common.interceptor.exception
 * @ClassName: MyExceptionHandler
 * @create: 2021-10-14 08:21
 **/
@RestControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler({Exception.class})
    @ResponseStatus
    public Result<Object> MyHandler(Exception e) {
        return Result.fail("系统错误："+e.getMessage());
    }


}
