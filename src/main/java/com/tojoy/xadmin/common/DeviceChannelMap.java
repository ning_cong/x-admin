package com.tojoy.xadmin.common;

import com.tojoy.xadmin.entity.IotDeviceEntity;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @description: 存放用户与通道的关联
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.common
 * @ClassName: DeviceChannelMap
 * @create: 2021-10-21 10:02
 **/
public abstract class DeviceChannelMap {

    /**
     * 存储用户对应的通道
     */
    private static Map<String, ChannelHandlerContext> MAP = new ConcurrentHashMap<>(16);

    /**
     * 存放通道到用户关联
     */
    private static Map<String,String> CHANNEL_USER = new ConcurrentHashMap<>(16);

    /**
     * 存储当前连接上的通道
     */
    private static List<ChannelHandlerContext> LIST = new CopyOnWriteArrayList<>();

    /**
     * 存储当前在线设备
     *
     */
    private static List<IotDeviceEntity> iotDeviceEntityList =  new CopyOnWriteArrayList<>();


    public static Map<String, ChannelHandlerContext> getMAP() {
        return MAP;
    }


    public static List<IotDeviceEntity> getIotDeviceList() {
        return iotDeviceEntityList;
    }

    public static Map<String, String> getChannelUser() {
        return CHANNEL_USER;
    }

    public static List<ChannelHandlerContext> getLIST() {
        return LIST;
    }
}
