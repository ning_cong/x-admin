package com.tojoy.xadmin.config.webconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.config
 * @ClassName: MyWebConfig
 * @create: 2021-10-12 15:14
 **/
@Configuration
public class MyWebConfig implements WebMvcConfigurer {
    @Autowired
    @Qualifier(value = "loginInterceptor")
    private HandlerInterceptor handlerInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(handlerInterceptor);
        //拦截请求
        interceptorRegistration.addPathPatterns("/**");

        //放行请求
        interceptorRegistration.excludePathPatterns(
                "/login",
                "/websocket",
                "/mqtt/webhook",
                "/user/login",
                "/user/logout",
                "/layui/**",
                "/lib/**",
                "/webjars/**",
                "/captcha",
                "/css/**",
                "/js/**",
                "/static/js/**",
                "/api/**",
                "/images/**"
                );
    }

//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry
//                //需要跨域的接口，例如：/openApi/*
//                .addMapping("/*")
//                //允许调用的域，例如：http://127.0.0.1:8888
//                .allowedOriginPatterns("*")
//                //接口调用方式，POST、GET等
//                .allowedMethods("*")
//                .allowedHeaders("*")
//                .allowCredentials(true);
//    }
}
