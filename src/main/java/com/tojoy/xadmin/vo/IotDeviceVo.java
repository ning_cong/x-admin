package com.tojoy.xadmin.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.vo
 * @ClassName: IotDeviceVo
 * @create: 2021-10-29 13:31
 **/
@Data
public class IotDeviceVo implements Serializable {

    /**
     * 序号
     */
    private Long deviceId;

    /**
     * 编号
     */
    private String deviceNum;


    /**
     * 名称
     */
    private String deviceName;


    /**
     * 用户
     */
    private String ownerId;


    /**
     * 删除标志（0代表存在 1代表删除）
     */
    private String isDeleted;


    /**
     * 转子号
     */
    private Integer rotorNum;
    /**
     * 速度
     */
    private Double speed;
    /**
     * 离心力
     */
    private String centrifugalForce;
    /**
     * 时间
     */
//    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date time;
    /**
     * 温度
     */
    private String temperature;
}

