package com.tojoy.xadmin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.vo
 * @ClassName: Page
 * @create: 2021-10-12 16:22
 **/
@Data
public class Page implements Serializable {
    private Integer page;
    private Integer limit;
    public Long getStart() {
        return (page -1L) *limit;
    }
}
