package com.tojoy.xadmin.vo;

import io.netty.channel.ChannelHandlerContext;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: spring-boot-netty-socket
 * @packageName: com.tojoy.nettysocket.common.utils
 * @ClassName: CurrentDevice
 * @create: 2021-09-18 09:23
 **/
public class CurrentDevice {
    private static ChannelHandlerContext ctx;


    public static ChannelHandlerContext getCtx() {
        return ctx;
    }

    public static void setCtx(ChannelHandlerContext ctx) {
        CurrentDevice.ctx = ctx;
    }

}
