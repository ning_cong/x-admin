package com.tojoy.xadmin.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.vo
 * @ClassName: HistoryRecordVo
 * @create: 2021-11-12 11:04
 **/
@Data
public class HistoryRecordVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Integer id;
    /**
     *
     */
    private String deviceName;
    /**
     *
     */
    private String programType;
    /**
     *
     */
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     *
     */
    private Time workTime;
    /**
     *
     */
    private Double speed;
    /**
     *
     */
    private String centrifugalForce;
    /**
     *
     */
    private String temperature;
    /**
     *
     */
    private String runStatus;

}
