package com.tojoy.xadmin.vo;

import lombok.Data;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin.entity
 * @ClassName: IotDeviceStatus
 * @create: 2021-10-21 17:20
 **/
@Data
public class IotDeviceOnlineStatus {
    private String clientId;
    private String username;
    private String event;
}
