package com.tojoy.xadmin;

import com.tojoy.xadmin.common.util.PageUtils;
import com.tojoy.xadmin.service.IHistoryRecordService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: qnc
 * @Version: 1.0
 * @program: x-admin
 * @packageName: com.tojoy.xadmin
 * @ClassName: HistoryRecordTest
 * @create: 2021-11-11 08:06
 **/
@SpringBootTest
public class HistoryRecordTest {

    @Autowired
    IHistoryRecordService historyRecordService;

    @Test
    public void getPage() {
        Map<String, Object> map = new HashMap<>();
        map.put("page","1");
        map.put("limit","15");
        map.put("deviceId","2");

        PageUtils page = historyRecordService.queryHistoryPage(map);
       page.getList().forEach(System.out::println);
        System.out.println("数目是"+page.getTotalCount());

    }
}
