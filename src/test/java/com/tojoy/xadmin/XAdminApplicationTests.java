package com.tojoy.xadmin;

import com.tojoy.xadmin.emp.vo.EmpQuery;
import com.tojoy.xadmin.entity.UserEntity;
import com.tojoy.xadmin.mapper.device.IotDeviceMapper;
import com.tojoy.xadmin.service.IIotDeviceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;


@SpringBootTest
class XAdminApplicationTests {

    @Autowired
    IIotDeviceService iotDeviceService;
    @Autowired
    IotDeviceMapper iotDeviceMapper;

    @Test
    void contextLoads() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode("123456");
        String encode2 = passwordEncoder.encode("123456");
        System.out.println(encode);
        System.out.println(encode2);
    }

    @Test
    public void ServiceTest() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername("admin");
        EmpQuery query = new EmpQuery();


        Long num = iotDeviceService.getDeviceNum(query, userEntity);
        System.out.println(num);
    }
    @Test
    public void deleteById() {
        boolean row = iotDeviceService.removeByIds(Arrays.asList(3,4,5));
        System.out.println("影响行数"+row);
    }

}
